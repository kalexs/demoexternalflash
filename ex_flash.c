#include "ex_flash.h"
#include <stm32f0xx.h>

#define TRASH											0x00
#define _WRITE_EN									0x06
#define _WRITE_DISB								0x04
#define _WRITE_STATUS_REG					0x01
#define _READ_STATUS_REG					0x05
#define _PAGE_PROGRAM							0x02
#define _SECTOR_ERASE							0xD8
#define _BULK_ERASE								0xC7
#define _READ_DATA								0x03

#define _DATA_SIZE								0x20

void ex_flash_init(void)
{
	RCC->AHBENR |= RCC_AHBENR_GPIOAEN;
	RCC->AHBENR |= RCC_AHBENR_GPIOCEN;
	RCC->AHBENR |= RCC_AHBENR_GPIOBEN;

	//SPI1 CLK, IN, OUT	
	GPIOB->MODER &= ~( GPIO_MODER_MODER3 | GPIO_MODER_MODER4 | GPIO_MODER_MODER5 );
	GPIOB->OTYPER &= ~( GPIO_OTYPER_OT_3 | GPIO_OTYPER_OT_4 | GPIO_OTYPER_OT_5 );
	GPIOB->OSPEEDR |= GPIO_OSPEEDR_OSPEEDR3 | GPIO_OSPEEDR_OSPEEDR4 | GPIO_OSPEEDR_OSPEEDR5;
	GPIOB->PUPDR &= ~( GPIO_PUPDR_PUPDR3 | GPIO_PUPDR_PUPDR4 | GPIO_PUPDR_PUPDR5 );
	GPIOB->AFR[0] &= ~( GPIO_AFRL_AFRL3 | GPIO_AFRL_AFRL4 | GPIO_AFRL_AFRL5 );
	GPIOB->MODER |= GPIO_MODER_MODER3_1 | GPIO_MODER_MODER4_1 | GPIO_MODER_MODER5_1;

	//SPI1 CS
	GPIOA->MODER &= ~( GPIO_MODER_MODER15 );
	GPIOA->MODER |= GPIO_MODER_MODER15_0;
	GPIOA->OTYPER &= ~( GPIO_OTYPER_OT_15 );
	GPIOA->OSPEEDR |= GPIO_OSPEEDR_OSPEEDR15;
	GPIOA->PUPDR &= ~( GPIO_PUPDR_PUPDR15 );

	RCC->APB2ENR |= RCC_APB2ENR_SPI1EN;
	SPI1->CR1 |= SPI_CR1_SSM; // slave select manager - software by SSI
	SPI1->CR1 |= SPI_CR1_SSI;
	SPI1->CR1 |= ( SPI_CR1_BR_0 | SPI_CR1_BR_1 | SPI_CR1_BR_2 );//serial clock band rate 256
	SPI1->CR1 |= SPI_CR1_CPOL; //high-level idle state
	SPI1->CR1 |= SPI_CR1_CPHA; //rising edge
	SPI1->CR1 |= SPI_CR1_MSTR; //master
	SPI1->CR2 &= ~( SPI_CR2_DS_0 | SPI_CR2_DS_1 | SPI_CR2_DS_2 | SPI_CR2_DS_3 );
	SPI1->CR2 |= SPI_CR2_DS_0 | SPI_CR2_DS_1 | SPI_CR2_DS_2 | SPI_CR2_FRXTH;
	SPI1->CR1 |= SPI_CR1_SPE; //SPI2 enable
}

uint8_t sendByte_spi1(uint8_t send_data)
{
	uint8_t * _DR = (uint8_t*)( &SPI1->DR );
	uint8_t rec_data = 0;

	while ( !( SPI1->SR & SPI_SR_TXE ) )
		;

	*_DR = send_data;

	while ( !( SPI1->SR & SPI_SR_RXNE ) )
		;

	rec_data = *_DR;

	return rec_data;
}

 uint8_t acc_read1_spi1(uint8_t addr)
 {
 	uint8_t send_addr = 0;
 	uint8_t rec_data = 0;
 
 	send_addr |= 1 << 7; //receive data
 	send_addr |= addr;
 
 	GPIOA->BSRR = GPIO_BSRR_BR_15;
 	sendByte_spi1(send_addr);
 	rec_data = sendByte_spi1(TRASH);
 	GPIOA->BSRR = GPIO_BSRR_BS_15;
 
 	return rec_data;
 }

void ex_flash_write_enable ()
{
	GPIOA->BSRR = GPIO_BSRR_BR_15;
	sendByte_spi1(_WRITE_EN);
	GPIOA->BSRR = GPIO_BSRR_BS_15;
}

void ex_flash_write_disable ()
{
	GPIOA->BSRR = GPIO_BSRR_BR_15;
	sendByte_spi1(_WRITE_DISB);
	GPIOA->BSRR = GPIO_BSRR_BS_15;
}

void ex_flash_write_status_reg(uint8_t data)
{
	ex_flash_write_enable();
	
	GPIOA->BSRR = GPIO_BSRR_BR_15;
	sendByte_spi1(_WRITE_STATUS_REG);
	sendByte_spi1(data);
	GPIOA->BSRR = GPIO_BSRR_BS_15;
	
	ex_flash_write_disable();
}

uint8_t ex_flash_read_status_reg()
{
	uint8_t data = 0x00;
	
	ex_flash_write_enable();
	
	GPIOA->BSRR = GPIO_BSRR_BR_15;
	sendByte_spi1(_WRITE_STATUS_REG);
	data = sendByte_spi1(TRASH);
	GPIOA->BSRR = GPIO_BSRR_BS_15;
	
	ex_flash_write_disable();
	
	return data;
}

void ex_flash_write_cell(uint32_t addr, uint8_t * data, uint16_t length)
{
	
	ex_flash_write_enable();
	
	GPIOA->BSRR = GPIO_BSRR_BR_15;
	sendByte_spi1(_PAGE_PROGRAM);
	
	sendByte_spi1(addr>>12);
	sendByte_spi1(addr>>8);
	sendByte_spi1(addr);
	
	for (uint16_t i = 0; i < length; i++)
	{
		sendByte_spi1(data[i]);
	}
	GPIOA->BSRR = GPIO_BSRR_BS_15;
	
	ex_flash_write_disable();
}

void ex_flash_sector_erase(uint32_t addr)
{
	
	ex_flash_write_enable();
	
	GPIOA->BSRR = GPIO_BSRR_BR_15;
	sendByte_spi1(_SECTOR_ERASE);
	
	sendByte_spi1(addr>>12);
	sendByte_spi1(addr>>8);
	sendByte_spi1(addr);
	
	GPIOA->BSRR = GPIO_BSRR_BS_15;
	
	ex_flash_write_disable();
}

void ex_flash_bulk_erase()
{
	
	ex_flash_write_enable();
	
	GPIOA->BSRR = GPIO_BSRR_BR_15;
	sendByte_spi1(_BULK_ERASE);
	GPIOA->BSRR = GPIO_BSRR_BS_15;
	
	ex_flash_write_disable();
}

uint8_t* ex_flash_read_data(uint32_t addr)
{
	uint8_t * data;
	
	ex_flash_write_enable();
	
	GPIOA->BSRR = GPIO_BSRR_BR_15;
	sendByte_spi1(_READ_DATA);
	
	sendByte_spi1(addr>>12);
	sendByte_spi1(addr>>8);
	sendByte_spi1(addr);
	
	for (uint16_t i =0; i < _DATA_SIZE; i++)
		data[i] = sendByte_spi1(TRASH);
		
	
	GPIOA->BSRR = GPIO_BSRR_BS_15;
	
	ex_flash_write_disable();
	
	return data;
}

