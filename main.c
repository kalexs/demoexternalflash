#include <stm32f0xx.h>
#include "rtc.h"
#include "time.h"

#define _BUFFER_SIZE 256

volatile static uint8_t buffer[2][_BUFFER_SIZE] = {"", ""};
volatile uint8_t current_buffer = 0;
volatile static uint8_t iter[2] = {0,0};
volatile static uint8_t is_transmit[2] = {0,0};

void push_char (uint8_t data)
{
	buffer[current_buffer][iter[current_buffer]] = data;
	iter[current_buffer]++;
	if (iter[current_buffer] > (_BUFFER_SIZE-1))
		iter[current_buffer]=0;
}

void send_part (uint8_t part)
{
//		while(!(USART1->ISR & USART_ISR_TC));
//			USART1->TDR = (((part >> 4) & 0x0F) + 0x30);
//		
//		while(!(USART1->ISR & USART_ISR_TC));
//			USART1->TDR = ((part & 0x0F) + 0x30);	
	
	push_char (((part >> 4) & 0x0F) + 0x30);
	push_char((part & 0x0F) + 0x30);
}

void USART1_IRQHandler (void)
{
	uint8_t t_buffer = 0;
	if (current_buffer == 0)
		t_buffer = 1;
	else
		t_buffer = 0;
	
	if (buffer[t_buffer][iter[t_buffer]] == 0x0D)
	{
		while(!(USART1->ISR & USART_ISR_TC));
			USART1->TDR = (buffer[t_buffer][iter[t_buffer]]);	
		
		iter[t_buffer] = 0;
		USART1->CR1 &= ~(USART_CR1_TXEIE);
		
		is_transmit[t_buffer] = 0;
	}
	else
	{
		while(!(USART1->ISR & USART_ISR_TC));
			USART1->TDR = (buffer[t_buffer][iter[t_buffer]]);	
		
		iter[t_buffer]++;
	}
}


int main(void)
{
	RCC->AHBENR |= RCC_AHBENR_GPIOCEN;
	RCC->AHBENR |= RCC_AHBENR_GPIODEN;
	RCC->AHBENR |= RCC_AHBENR_GPIOAEN;
	
	// USART_MODBUS (USART1) Tx=PA9 Rx=PA10
	GPIOA->MODER &= ~( GPIO_MODER_MODER9 | GPIO_MODER_MODER10 );
	GPIOA->MODER |= GPIO_MODER_MODER9_1 | GPIO_MODER_MODER10_1;
	GPIOA->OTYPER &= ~( GPIO_OTYPER_OT_9 );
	GPIOA->PUPDR &= ~( GPIO_PUPDR_PUPDR9 | GPIO_PUPDR_PUPDR10 );
	GPIOA->OSPEEDR &= ~( GPIO_OSPEEDER_OSPEEDR9 );
	GPIOA->AFR[1] &= ~( GPIO_AFRH_AFRH1 | GPIO_AFRH_AFRH2 );
	GPIOA->AFR[1] |= ( 1 << ( 4 * ( 9 - 8 ) ) );
	GPIOA->AFR[1] |= ( 1 << ( 4 * ( 10 - 8 ) ) );

	// USART_MODBUS (USART1) RTS=PA12
	GPIOA->MODER &= ~( GPIO_MODER_MODER12 );
	GPIOA->MODER |= GPIO_MODER_MODER12_1;
	GPIOA->OTYPER &= ~( GPIO_OTYPER_OT_12 );
	GPIOA->PUPDR &= ~( GPIO_PUPDR_PUPDR12 );
	GPIOA->OSPEEDR &= ~GPIO_OSPEEDER_OSPEEDR12;
	GPIOA->AFR[1] &= ~( GPIO_AFRH_AFRH4 );
	GPIOA->AFR[1] |= ( 1 << ( 4 * ( 12 - 8 ) ) );
	

	NVIC_EnableIRQ(USART1_IRQn);

	RCC->APB2ENR |= RCC_APB2ENR_USART1EN;
	USART1->CR1 |= 
	USART_CR1_DEAT_0 | USART_CR1_DEAT_1 | USART_CR1_DEAT_2 | USART_CR1_DEAT_3 | USART_CR1_DEAT_4 | 
	USART_CR1_DEDT_0 | USART_CR1_DEDT_1 | USART_CR1_DEDT_2 | USART_CR1_DEDT_3 | USART_CR1_DEDT_4 | 
	USART_CR1_TE | USART_CR1_RE; 
	USART1->BRR = SystemCoreClock / 9600;
	USART1->CR1 |= USART_CR1_UE;
	
	
	uint32_t date = 0;
	uint32_t time = 0;
	
	timer_init();
	rtc_init();
	
	while (1)
	{
		time = rtc_get_time();
		date = rtc_get_date();
		
		uint8_t minutes = time >> 8;
		uint8_t seconds = time;
		uint8_t hours = time >> 16;
		
		uint8_t day = date;
		uint8_t month = (date >> 8) & 0x1F;
		uint8_t year = (date >> 16);
		
		
		send_part (day);
		push_char(0x2D);
		send_part (month);
		push_char(0x2D);			
		send_part (year);
		push_char(0x09);
		
		send_part (hours);
		push_char(0x3A);
		send_part (minutes);
		push_char(0x3A);
		send_part (seconds);
		push_char(0x2E);
		push_char(rtc_get_subseconds() + 0x30);
		push_char(0x09);
		
		push_char(0x44);
		push_char(0x49);
		push_char(0x3A);
		push_char(0x20);
		push_char(0x46);
		push_char(0x46);
		push_char(0x09);
		
		push_char(0x44);
		push_char(0x4F);
		push_char(0x3A);
		push_char(0x20);
		push_char(0x46);
		push_char(0x46);
		push_char(0x09);
		
		
		push_char(0x43);
		push_char(0x55);
		push_char(0x52);
		push_char(0x52);
		push_char(0x3A);
		push_char(0x20);
		push_char(0x46);
		push_char(0x46);			
		push_char(0x0D);
			
		if (current_buffer == 0)
		{
			iter[current_buffer] = 0;
			if (!(is_transmit[1]))
			{
				current_buffer = 1;
				is_transmit[0] = 1;
				USART1->CR1 |= USART_CR1_TXEIE;
			}
		}
		else
		{
			iter[current_buffer] = 0;
			if (!(is_transmit[0]))
			{
				current_buffer = 0;
				is_transmit[1] = 1;
				USART1->CR1 |= USART_CR1_TXEIE;
			}
		}
	}
}
