#include "rtc.h"
#include <stm32f0xx.h>

#define _DEFAULT_TIME 0x203000
#define _DEFAULT_DATE 0x161019

void rtc_set_time (uint32_t date, uint32_t time)
{
	RTC->WPR = 0xCA;
	RTC->WPR = 0x53;
	RTC->ISR |= RTC_ISR_INIT;
	while ((RTC->ISR & RTC_ISR_INITF) != RTC_ISR_INITF)
	{
	}
	RTC->PRER = 0x007F0137;
	RTC->TR = time;
	RTC->DR = date;
	RTC->CR |= RTC_CR_BYPSHAD;
	RTC->ISR &=~ RTC_ISR_INIT;
	RTC->WPR = 0xFE;
	RTC->WPR = 0x64;
	
}

void rtc_init ()
{

		RCC->CSR |= RCC_CSR_LSION;
		while ( !(RCC->CSR & RCC_CSR_LSIRDY) ){}
		RCC->APB1ENR |= RCC_APB1ENR_PWREN;
		PWR->CR |= PWR_CR_DBP;
		RCC->BDCR |= RCC_BDCR_RTCSEL_LSI;
		RCC->BDCR |= RCC_BDCR_RTCEN;
		//PWR->CR &= ~(PWR_CR_DBP);
			
	if (RTC->DR == 0x2101)
  {
		rtc_set_time(_DEFAULT_DATE, _DEFAULT_TIME);
	}
}

uint32_t rtc_get_time ()
{
	uint32_t time = 0;
	
	//if((RTC->ISR & RTC_ISR_RSF) == RTC_ISR_RSF)
	{
		time = RTC->TR; /* get time */
	}
	
	return time;
}

uint32_t rtc_get_date ()
{
	uint32_t date = 0;
	
	//if((RTC->ISR & RTC_ISR_RSF) == RTC_ISR_RSF)
	{
		date = RTC->DR; /* get time */
	}
	
	return date;
}

uint8_t rtc_get_subseconds ()
{
	uint8_t sub = 0;
	//if((RTC->ISR & RTC_ISR_RSF) == RTC_ISR_RSF)
	{
		sub = 10 * ((0x007F0137 & RTC_PRER_PREDIV_S) - (RTC->SSR & RTC_PRER_PREDIV_S)) / ((0x007F0137 & RTC_PRER_PREDIV_S) + 1);
	}
	return sub;
}
