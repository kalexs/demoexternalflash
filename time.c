#include <stm32f0xx.h>
#include "time.h"

volatile static uint16_t _wait_count = 0;
volatile static uint16_t _count = 0;
volatile static uint16_t _timestamp = 0;

void timer_init(void)
{
	SystemCoreClockUpdate();

	SysTick->CTRL = SysTick->CTRL | SysTick_CTRL_CLKSOURCE_Msk | SysTick_CTRL_ENABLE_Msk | SysTick_CTRL_TICKINT_Msk;
	SysTick->LOAD = SystemCoreClock / 1000 - 1;
}


void SysTick_Handler(void)
{
	_count++;
	_wait_count++;
}

void timer_wait(uint16_t period)
{
	_wait_count = 0;
	while ( _wait_count < period )
		;
}

void timer_start(void)
{
	_timestamp = _count;
}

int16_t timer_stop(void)
{
	return ( _count - _timestamp );
}
