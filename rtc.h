#ifndef CEDR_EXTERNAL_RTC_H
#define CEDR_EXTERNAL_RTC_H

#include <stdint.h> // int16_t

void rtc_set_time (uint32_t date, uint32_t time);
void rtc_init (void);
uint32_t rtc_get_time (void);
uint32_t rtc_get_date (void);
uint8_t rtc_get_subseconds (void);

#endif // !CEDR_EXTERNAL_RTC_H
