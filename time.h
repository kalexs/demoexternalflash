#ifndef CEDR_MODBUS_IO_TIME_H
#define CEDR_MODBUS_IO_TIME_H

#include <stdint.h>  // int16_t

void timer_init(void);
void timer_wait(uint16_t period);
void timer_start(void);
int16_t timer_stop(void);

#endif // !CEDR_MODBUS_IO_TIME_H
