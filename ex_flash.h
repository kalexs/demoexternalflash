#ifndef CEDR_EXTERNAL_FLASH_EX_FLASH_H
#define CEDR_EXTERNAL_FLASH_EX_FLASH_H

#include <stdint.h> // int16_t

void ex_flash_write_status_reg(uint8_t data);
uint8_t ex_flash_read_status_reg(void);
void ex_flash_write_cell(uint32_t addr, uint8_t * data, uint16_t length);
void ex_flash_sector_erase(uint32_t addr);
void ex_flash_bulk_erase(void);

#endif // !CEDR_EXTERNAL_FLASH_EX_FLASH_H
